var browserSync    = require('browser-sync')
var critical       = require('critical')
var del            = require('del')
var fs             = require('fs')
var gulp           = require('gulp')
var autoprefixer   = require('gulp-autoprefixer')
var cache          = require('gulp-cache')
var changed        = require('gulp-changed')
var cleanCSS       = require('gulp-clean-css')
var concat         = require('gulp-concat')
var gulpIf         = require('gulp-if')
var imagemin       = require('gulp-imagemin')
var jpegRecompress = require('imagemin-jpeg-recompress')
var purifycss      = require('gulp-purifycss')
var sass           = require('gulp-sass')
var sourcemaps     = require('gulp-sourcemaps')
var uglify         = require('gulp-uglify')
var useref         = require('gulp-useref')
var runSequence    = require('run-sequence')
var webp           = require('gulp-webp')
var wiredep        = require('wiredep').stream

// custom modules
var config = require('./config.js')
var flags = { production: false }


// Development Tasks 
// -----------------
// Start browserSync server
gulp.task('browserSync', function() {
  server = config.sync.serverboolean
  if (server == true) {
    browserSync.init(config.sync.server)
  } else {
    browserSync(config.sync.noserver)
  }
})
// Sass convert
gulp.task('sass', function() {
  return gulp.src(config.devPaths.scss + '**/*.scss')
    .pipe(gulpIf(!flags.production, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: config.settingsAutoprefixer.browsers }))
    .pipe(gulpIf(!flags.production, sourcemaps.write()))
    .pipe(changed(config.devPaths.css, { hasChanged: changed.compareSha1Digest }))
    .pipe(gulp.dest(config.devPaths.css))
    .pipe(browserSync.reload({ stream: true }))
})
// Automatically inject Less and Sass Bower dependencies
gulp.task('bowerStyles', function () {
  return gulp.src(config.devPaths.allCss)
    .pipe(wiredep(config.bowerFiles))
    .pipe(gulp.dest(config.devPaths.scss))
})
// Automatically inject js
gulp.task('bowerScripts', function () {
  return gulp.src(config.devPaths.headerTpl)
    .pipe(wiredep(config.bowerFiles))
    .pipe(gulp.dest(config.devPaths.headerFolder))
})
// Copy-paste fontawesome
gulp.task('fontAwesome', function() {
  return gulp.src('bower_components/font-awesome/fonts/**/*.{woff,woff2}')
    .pipe(gulp.dest(config.devPaths.fonts))
})
gulp.task('clean:webp', function() {
  return del.sync(config.devPaths.images + 'webp')
})
//Convert to webp
gulp.task('convertImageToWebp', function() {
  return gulp.src([config.devPaths.images + '**/*.{png,jpg,jpeg}', '!webp'])
    .pipe(webp({quality: 100}))
    .pipe(gulp.dest(config.devPaths.images + '/webp'))
})
// Clean and convert webp
gulp.task('imageToWebp', function(callback) {
  runSequence('clean:webp', 'convertImageToWebp',
    callback
  )
})
// Bower tasks
gulp.task('bower', function(callback) {
  runSequence('bowerStyles', 'bowerScripts', 'fontAwesome',
    callback
  )
})
// Watchers
gulp.task('watch', function() {
  gulp.watch('*.css', ['sass'])
  gulp.watch(config.devPaths.scss + '**/*.scss', ['sass'])
  gulp.watch(config.devPaths.scripts + '**/*.js', browserSync.reload)
  gulp.watch(config.devPaths.html + '**/*.{php,tpl,html}', browserSync.reload)
  gulp.watch(config.devPaths.images + '**/*.{png,jpg,jpeg}', ['imageToWebp'])
})



// Production Tasks
// -----------------
// Clean before production
gulp.task('clean:dist', function() {
  return del.sync(config.distPaths.root)
})
// Create custom bootstrap.js
gulp.task('customBootstrap', function() {
  return gulp.src(config.bootstrapComponents)
    .pipe(concat('bootstrap.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.bowerBootstrapDist))
})
// Contcatenation scripts
gulp.task('useref', function() {
  return gulp.src(config.devPaths.headerTpl)
    .pipe(useref(config.useref))
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest(config.distPaths.headerFolder))
})
// Optimizing Images 
gulp.task('images', function() {
  return gulp.src(config.devPaths.images + '**/*')
    .pipe(cache(imagemin([
      imagemin.gifsicle(),
      jpegRecompress({
        loops:4,
        min: 50,
        max: 95,
        quality:'high' 
      }),
      imagemin.optipng(),
      // imagemin.svgo()
    ])))
    .pipe(gulp.dest(config.distPaths.images))
})
gulp.task('clear', function (done) {//clear cache with images - if needs
    return cache.clearAll(done)
})
// Copy-paste fonts
gulp.task('fonts', function() {
  return gulp.src(config.devPaths.fonts + '**/*.{woff,woff2,otf}')
  .pipe(gulp.dest(config.distPaths.fonts))
})
// Remove unnecessary css
gulp.task('removeUnnecessaryCss', function() {
  return gulp.src(config.distPaths.css + '**/*.css')
    .pipe(purifycss(config.purifyCssSrc))
    .pipe(gulp.dest(config.distPaths.css))
})
// Minify css
gulp.task('minify', function() {
  return gulp.src(config.distPaths.css + '**/*.css')
    .pipe(cleanCSS({level:{1:{specialComments:0}}}))
    .pipe(gulp.dest(config.distPaths.css))
})
// critical css
gulp.task('critical', function (cb) {
    for (i = 0; i < config.criticalSrcPages.length; ++i) {
      critical.generate({
        base: config.critical.base,
        inline: config.critical.inline,
        ignore: config.critical.ignore,
        src: config.criticalSrcPages[i].url,
        css: config.critical.css,
        dest: config.criticalSrcPages[i].css,
        minify: config.critical.minify,
        width: config.critical.width,
        height: config.critical.height
      })
    }
})

// service links
gulp.task('firsttask', function() {
  fs.readFile(config.serviceLinks.url, 'utf8', function ( err, data ) {
    fs.writeFile(config.serviceLinks.url, data.replace('</head>', config.serviceLinks.links))
  })
})



//Default task - dev
gulp.task('default', function(callback) {
  runSequence(
    'bower',
    'sass',
    'browserSync',
    'imageToWebp',
    ['watch'],
    callback
  )
})
gulp.task('build', function(callback) {
  flags.production = true
  runSequence(
    'clean:dist',
    'sass',
    'customBootstrap',
    'imageToWebp',
    ['useref', 'images', 'fonts'],
    'removeUnnecessaryCss',
    'minify',
    'critical',
    callback
  )
})