// path for folder/files
var devPaths = {
  allCss: 'src/scss/bower.scss',
  scss: 'src/scss/',
  css: 'src/css/',
  scripts: 'src/js/',
  images: 'src/img/',
  fonts: 'src/fonts/',
  html: 'src/',
  headerFolder: 'src/',
  headerTpl: 'src/*.html'
}
var distPaths = {
  root: 'dist/',
  css: 'dist/css/',
  scripts: 'dist/js/',
  images: 'dist/img/',
  fonts: 'dist/fonts/',
  html: 'dist/',
  headerFolder: 'dist/'
}


// browserSync
var sync = {
  serverboolean: false,
  server: {
    files: ['{template-parts}/**/*.php', '*.php'],
    proxy: 'http://',
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  },
  noserver: {
    server: {
      baseDir: "src/",
      routes: {"/bower_components": "bower_components"}
    }
  }
}


// change paths, exclude files from bower
var bowerFiles = {
  fileTypes: {
    html: {
      replace: {
        // js: '<script src="/wp-content/themes/CF_blog/{{filePath}}"></script>',
        // css: '<link rel="stylesheet" href="/wp-content/themes/CF_blog/{{filePath}}" />'
      }
    }
  },
  exclude: [
    // 'bower_components/tether/dist/js/tether.js',
    // 'bower_components/bootstrap/dist/js/bootstrap.js'
  ]
}


// custom bootstrap
var bowerBootstrapSrc = 'bower_components/bootstrap/js/dist/'
var bowerBootstrapDist = 'bower_components/bootstrap/dist/js/'
var bootstrapComponents = [
  bowerBootstrapSrc + 'util.js',
  bowerBootstrapSrc + 'alert.js',
  // bowerBootstrapSrc + 'button.js',
  // bowerBootstrapSrc + 'carousel.js',
  bowerBootstrapSrc + 'collapse.js',
  bowerBootstrapSrc + 'dropdown.js',
  bowerBootstrapSrc + 'modal.js',
  // bowerBootstrapSrc + 'popover.js',
  // bowerBootstrapSrc + 'scrollspy.js',
  // bowerBootstrapSrc + 'tab.js',
  // bowerBootstrapSrc + 'tooltip.js'
]


// combine files
var useref = {//on real projects with backend may be issues
  transformPath: function(filePath) {//this is example how to fix paths when files combining
    return filePath.replace('/wp-content/','../../')
  }
}


// remove css
var purifyCssSrc = [
  devPaths.html + '**/*.html',
  distPaths.scripts + '**/*.js'
]


// critical css
var criticalSrcPages = [
    // {
    //   url: '404.html',
    //   css: '404.html'
    // },
    // {
    //   url: 'category_overview_page.html',
    //   css: 'category_overview_page.html'
    // },
    // {
    //   url: 'search_results_page.html',
    //   css: 'search_results_page.html'
    // },
    // {
    //   url: 'artist_landing_page.html',
    //   css: 'artist_landing_page.html'
    // },

    // {
    //   url: 'buying_options.html',
    //   css: 'buying_options.html'
    // },
    // {
    //   url: 'cart.html',
    //   css: 'cart.html'
    // },
    // {
    //   url: 'check_out.html',
    //   css: 'check_out.html'
    // },
    // {
    //   url: 'confirmation.html',
    //   css: 'confirmation.html'
    // },

    // {
    //   url: 'D_landing_page.html',
    //   css: 'D_landing_page.html'
    // },
    // {
    //   url: 'help_pages.html',
    //   css: 'help_pages.html'
    // },
    // {
    //   url: 'home_PDP.html',
    //   css: 'home_PDP.html'
    // },
    // {
    //   url: 'login.html',
    //   css: 'login.html'
    // },


    //4
    // {
    //   url: 'my_account_my_addresses.html',
    //   css: 'my_account_my_addresses.html'
    // },
    // {
    //   url: 'my_account_my_order(NOT_logged).html',
    //   css: 'my_account_my_order(NOT_logged).html'
    // },
    // {
    //   url: 'my_account_my_order.html',
    //   css: 'my_account_my_order.html'
    // },


    //5
    // {
    //   url: 'my_account_my_reviews-empty.html',
    //   css: 'my_account_my_reviews-empty.html'
    // },
    // {
    //   url: 'my_account_my_watchlist.html',
    //   css: 'my_account_my_watchlist.html'
    // },
    // {
    //   url: 'my_account_my_watchlist_empty.html',
    //   css: 'my_account_my_watchlist_empty.html'
    // },
    // {
    //   url: 'my_account_reset_password.html',
    //   css: 'my_account_reset_password.html'
    // },


    //6
    // {
    //   url: 'my_account_sessions.html',
    //   css: 'my_account_sessions.html'
    // },
    // {
    //   url: 'my_acсount_change_password.html',
    //   css: 'my_acсount_change_password.html'
    // },
    // {
    //   url: 'my_empty_cart.html',
    //   css: 'my_empty_cart.html'
    // },
    // {
    //   url: 'my_page_overview.html',
    //   css: 'my_page_overview.html'
    // },

    //7
    // {
    //   url: 'overview.html',
    //   css: 'overview.html'
    // },
    // {
    //   url: 'payment.html',
    //   css: 'payment.html'
    // },
    // {
    //   url: 'registration.html',
    //   css: 'registration.html'
    // },

    //8
    // {
    //   url: 'product_detail_page.html',
    //   css: 'product_detail_page.html'
    // },
    // {
    //   url: 'search_no_result.html',
    //   css: 'search_no_result.html'
    // },

    //9
    // {
    //   url: 'seller_page_help.html',
    //   css: 'seller_page_help.html'
    // },
    // {
    //   url: 'seller_page_policies.html',
    //   css: 'seller_page_policies.html'
    // },
    // {
    //   url: 'seller_page_return_address.html',
    //   css: 'seller_page_return_address.html'
    // },
    // {
    //   url: 'seller_page_returns_refunds.html',
    //   css: 'seller_page_returns_refunds.html'
    // },

    //10
    // {
    //   url: 'seller_page_seller_overview.html',
    //   css: 'seller_page_seller_overview.html'
    // },
    // {
    //   url: 'shipping-billing.html',
    //   css: 'shipping-billing.html'
    // },
    // {
    //   url: 'sold_out.html',
    //   css: 'sold_out.html'
    // },
    // {
    //   url: 'text_pages.html',
    //   css: 'text_pages.html'
    // },
    // {
    //   url: 'watchlist.html',
    //   css: 'watchlist.html'
    // },
    
]
var critical = {
  base: 'dist/',
  // inline: false,
  inline: true,
  ignore: ['@font-face',/url\(/, /.modal/, /*/.dropdown/*/],
  css: [
    'src/css/bower.css',
    'src/css/style.css'
  ],
  minify: true,
  width: 1300,
  height: 750
}


// autoprefixer
var settingsAutoprefixer = {
  browsers: [
    'last 2 versions',
    'safari >= 3.1',
    'android 4'
  ]
}


// service links
var serviceLinks = {
  url: devPaths.headerTpl,
  links: 
    '\n' +
    '<!-- build:css css/all.css -->\n' +
    '<link rel="stylesheet" href="css/bower.css">\n' +
    '<!-- bower:css -->\n' +
    '<!-- endbower -->\n' +
    '<link rel="stylesheet" href="css/style.css">\n' +
    '<!-- endbuild -->\n' +
    '\n' +
    '<!-- build:js js/all.js defer -->\n' +
    '<!-- bower:js -->\n' +
    '<!-- endbower -->\n' +
    '<script src="js/script.js"></script>\n' +
    '<!-- endbuild -->\n' +
    '</head>'
}


module.exports = {
    criticalSrcPages, criticalSrcPages,
    critical: critical,
    bootstrapComponents: bootstrapComponents,
    bowerBootstrapDist: bowerBootstrapDist,
    bowerFiles: bowerFiles,
    devPaths: devPaths,
    distPaths: distPaths,
    purifyCssSrc: purifyCssSrc,
    serviceLinks: serviceLinks,
    settingsAutoprefixer: settingsAutoprefixer,
    sync: sync
}