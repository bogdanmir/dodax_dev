$(document).ready(function() {
	$(document).on("click", ".preventDef", function( event ) {
		event.preventDefault();
	});
	function fromHeaderForMobile (){
		var arrow = $(".arrow.right");
		if (window.matchMedia('(max-width: 991px)').matches) {
			$('.links_top_header:first-child li').detach().appendTo('.m_from_header');
			if ($(".m_from_header li a i.arrow").length <= 0) {
				$('.m_from_header li a').append("<i class=\"bg-dark arrow right d-ForRemove\"></i>");
			}
		} else{
			$('.m_from_header li').detach().prependTo('.top_header .links_top_header');
			$('.d-ForRemove').remove();
		};
	};

	function js_repos_buying(){
		if (window.matchMedia('(max-width: 991px)').matches) {
			$('.js_repos_buying').children().detach().appendTo('.repos_to_buying');
			$('.repos_last_buying').children().detach().appendTo('.last_repos_buying');
			$('.likeRepos').children().detach().appendTo('.reposLikeTo');
		} else{
			$('.repos_to_buying').children().detach().prependTo('.js_repos_buying');
			$('.last_repos_buying').children().detach().prependTo('.repos_last_buying');
			$('.reposLikeTo').children().detach().prependTo('.likeRepos');
		};
	};


	function js_repos_products(){
		if (window.matchMedia('(max-width: 767px)').matches) {
			$('.butReposBack').each(function(){
				var	pos1 = $(this).closest('.reposObject').find('.tobutReposBack');
				$(this).children().detach().appendTo(pos1);
			})	
			$('.ContReposButtons').each(function(){
				var	pos1 = $(this).closest('.reposObject').find('.toContReposButtons');
				$(this).children().detach().appendTo(pos1);
			})
		} else{
			$('.tobutReposBack').each(function(){
				var	pos1 = $(this).closest('.reposObject').find('.butReposBack');
				$(this).children().detach().appendTo(pos1);
			})	
			$('.toContReposButtons').each(function(){
				var	pos1 = $(this).closest('.reposObject').find('.ContReposButtons');
				$(this).children().detach().appendTo(pos1);
			})
		};
	};

	var swiper5 = undefined;
	function initSwiperCountry() {    
	    if (window.matchMedia('(max-width: 991px)').matches && swiper5 == undefined) {    
			if( $("*").is(".countryList") ) {
			    var swiper5 = new Swiper('.countryList', {
			        slidesPerView: 'auto',
			        spaceBetween: 10,
			        visibilityFullFit: true
				})
			}
		} else if (window.matchMedia('(max-width: 991px)').matches && swiper5 != undefined) {            
	        swiper5.destroy();
	        swiper5 = undefined;
	        console.log("destroy");
	        $('.countryList .swiper-wrapper').removeAttr('style');
	        $('.countryList .swiper-slide').removeAttr('style');            
	    }        
	}   

	var mySwiper = undefined;
	function initSwiperPdp() {
	    var screenWidth = $(window).width();        
	   if (window.matchMedia('(max-width: 991px)').matches && mySwiper == undefined) {    
		    if( $("*").is(".mob_slide_prod")){   
		    	var newsSlide = 0;
		        $('.mob_slide_prod').each(function(){
		        	newsSlide++;
		        	$(this).closest(".mob_slide_prod").find('.swiper_btn_prev').addClass("prevControll"+newsSlide);
					$(this).closest(".mob_slide_prod").find('.swiper_btn_next').addClass("nextControll"+newsSlide);
		            mySwiper = new Swiper(this, {  
		                direction: 'horizontal',
						initialSlide: 0,
						autoplay: false,
						slidesPerView: 1,
						spaceBetween: 40,
						nextButton: '.nextControll'+newsSlide,
						prevButton: '.prevControll'+newsSlide,
		            })
		        });        
		    }
	    } else if (window.matchMedia('(max-width: 991px)').matches && mySwiper != undefined) {            
	        mySwiper.destroy();
	        mySwiper = undefined;
	        console.log("destroy");
	        $('.mob_slide_prod .swiper-wrapper').removeAttr('style');
	        $('.mob_slide_prod .swiper-slide').removeAttr('style');            
	    }        
	}   

	function js_repos_autor(){
		if (window.matchMedia('(max-width: 991px)').matches) {
			$('.infReposPdp').each(function(){
				var	pos1 = $(this).closest('.frameReposAutor').find('.toinfReposPdp');
				$(this).children().detach().appendTo(pos1);
			})	
			$('.BigAutor').each(function(){
				var	pos1 = $(this).closest('.frameBigautor').find('.toBigAutor');
				$(this).children().detach().appendTo(pos1);
			})			
		} else{	
			$('.toinfReposPdp').each(function(){
				var	pos1 = $(this).closest('.frameReposAutor').find('.infReposPdp');
				$(this).children().detach().appendTo(pos1);
			})	
			$('.toBigAutor').each(function(){
				var	pos1 = $(this).closest('.frameBigautor').find('.BigAutor');
				$(this).children().detach().appendTo(pos1);
			})
		};
	};

	function js_repos_sort(){
		if (window.matchMedia('(max-width: 991px)').matches) {
			$('.reposToModalFilters').children().detach().appendTo('.tOreposToModalFilters');
			$('.allFiltersRepos').children().detach().appendTo('.tOAllFiltersRepos');
		} else{
			$('.tOreposToModalFilters').children().detach().prependTo('.reposToModalFilters');
			$('.tOAllFiltersRepos').children().detach().prependTo('.allFiltersRepos');
		};
	};

	function js_repos_buying2(){
		if (window.matchMedia('(max-width: 991px)').matches) {
			$('.js_mob_pdp_2_2').each(function(){
				var	pos1 = $(this).closest('.tr_buying').find('.js_mob_pdp_2');
				$(this).detach().appendTo(pos1);
			})	
			$('.js_mob_pdp_3_2').each(function(){
				var	pos1 = $(this).closest('.tr_buying').find('.js_mob_pdp_3');
				$(this).detach().appendTo(pos1);
			})
		} else{
			$('.js_mob_pdp_2_2').each(function(){
				var	pos1 = $(this).closest('.tr_buying').find('.js_mob_pdp_4');
				$(this).detach().appendTo(pos1);
			})	
			$('.js_mob_pdp_3_2').each(function(){
				var	pos1 = $(this).closest('.tr_buying').find('.js_mob_pdp_2');
				$(this).detach().appendTo(pos1);
			})
			$('.LastRep').each(function(){
				var	pos1 = $(this).closest('.tr_buying').find('.js_mob_pdp_2');
				$(this).detach().appendTo(pos1);
			})
		};
	};
	function overflowThreeLine(){
		if (window.matchMedia('(max-width: 767px)').matches) {
			$('.sm_overflow').each(function(){
				var	liheHeight = $(".sm_overflow").css('line-height').replace('px', '');
				var paddingTop = $(".sm_overflow").css('padding-top').replace('px', '');
				var maxHeight = liheHeight * 3 + +paddingTop;
				$('.sm_overflow').css({"max-height": maxHeight });
			})
		} else{
			$(".sm_overflow").css({'max-height':'none'});
		}
	};

	function overflowThreeLineDesc(){
		$('.sm_overflow').each(function(){
			var	liheHeight = $(".sm_overflow").css('line-height').replace('px', '');
			var paddingTop = $(".sm_overflow").css('padding-top').replace('px', '');
			var maxHeight = liheHeight * 3 + +paddingTop;
			$('.sm_overflow').css({"max-height": maxHeight });
		})
	};

	function cutTotalPrice(){
		$( ".orders .payment_total" ).each(function() {
			if (window.matchMedia('(max-width: 991px)').matches) {
				$(this).closest('.collapse').find('.pay_total_mob').append($(this).detach());
			} else{
				$(this).closest('.collapse').find('.pay_total_desktop').append($(this).detach());
			};
		});
	};
	function stickyHeader() {
		if (window.matchMedia('(min-width: 992px)').matches & $(document).scrollTop() >= 50) {
			$("body").addClass("sticky_header");
		}else{
			$("body").removeClass("sticky_header");
		}
	};


	function closeFilters() {
		if (window.matchMedia('(max-width: 991px)').matches) {
			$("ul.select-options").removeClass("active").attr('style', '');
		}else{
			if ($(".filter_modal").hasClass("show")) {
				$(".filter_modal").modal('hide');
			}
			if ($(".sort_by_modal").hasClass("show")) {
				$(".sort_by_modal").modal('hide');
			}
		}
	}
	if( $("*").is(".related_slider") ) {
		mySwiperM = new Swiper ('.related_slider', {
			direction: 'horizontal',
			initialSlide: 0,
			autoplay: false,
			slidesPerView: 6,
			keyboardControl:  true,
			loop: true,
			autoHeight:true,
			nextButton: '.next_product_slider',
			prevButton: '.prev_product_slider',
			breakpoints: {
				992: {
				  slidesPerColumnFill: "column",
				  slidesPerView: 6,
				},
				1200: {
				  slidesPerView: 5,
				}
			}
		})
	};
	if( $("*").is(".related4Slider") ) {
		mySwiperM4 = new Swiper ('.related4Slider', {
			direction: 'horizontal',
			initialSlide: 0,
			autoplay: false,
			slidesPerView: 4,
			keyboardControl:  true,
			loop: true,
			nextButton: '.next_product_slider',
			prevButton: '.prev_product_slider',
			breakpoints: {
				992: {
				  slidesPerView: 2,
				},
				1200: {
				  slidesPerView: 4,
				}
			}
		})
	};	
// slider for home_PDP
	if( $("*").is(".top_slider") ) {
		var mySwiper = new Swiper ('.top_slider', {
			direction: 'horizontal',
			initialSlide: 0,
			autoplay: false,
			slidesPerView: 1,
			paginationClickable: true,
			keyboardControl:  true,
			keyboardControl: true,
			paginationClickable: true,
			loop: true,
			pagination: '.p_top_slider',
			nextButton: '.next_top_slider',
			prevButton: '.prev_top_slider'
		})
	};

	// sliders for home_PDP
	var slideCol = 0;
	$(".slidersHome").each(function(){
		slideCol++;
		$(this).closest(".pdp_slide").find('.swiper_btn_prev').addClass("prevControll"+slideCol);
		$(this).closest(".pdp_slide").find('.swiper_btn_next').addClass("nextControll"+slideCol);
		var mySwiper = new Swiper ($(this), {
			direction: 'horizontal',
			initialSlide: 0,
			autoplay: false,
			slidesPerView: 5,
			spaceBetween: 5,
			keyboardControl:  true,
			loop: true,
			nextButton: '.nextControll'+slideCol,
			prevButton: '.prevControll'+slideCol,
			breakpoints: {
				640: {
				  slidesPerView: 2.5,
				  spaceBetween: 20
				},
				992: {
				  slidesPerView: 4,
				  spaceBetween: 20
				},
				1200: {
				  slidesPerView: 5,
				  spaceBetween: 20
				}
			}
		})
	})
	if( $("*").is(".confirm_slider") ) {
		var mySwiper1 = new Swiper ($(".confirm_slider"), {
			direction: 'horizontal',
			initialSlide: 0,
			autoplay: false,
			slidesPerView: 5,
			spaceBetween: 5,
			keyboardControl:  true,
			loop: true,
			nextButton: '.swiper_next_confirm',
			prevButton: '.swiper_prev_confirm',
			breakpoints: {
				640: {
				  slidesPerView: 2.5,
				  spaceBetween: 20
				},
				992: {
				  slidesPerView: 4,
				  spaceBetween: 20
				},
				1200: {
				  slidesPerView: 5,
				  spaceBetween: 20
				}
			}
		})
	};
	function goToTop() {
		if ($(document).scrollTop() >= 1500) {
			$(".goto_up").addClass("onScroll");
		}else{
			$(".goto_up").removeClass("onScroll");
		}
	};

	function reposeQuanity() {
		$( ".reposQuanity" ).each(function() {
			if (window.matchMedia('(max-width: 991px)').matches) {
				$(this).closest('.info_overview').find('.toReposQuanity').append($(this).children().detach());
			} else{
				$(this).closest('.info_overview').find('.backRepos').append($(this).children().detach());
			};
		});
	}

	function stopScrollMobile() {
		if ($("*").is(".CustomScroll")) {
			if (window.matchMedia('(max-width: 991px)').matches) {
				$(".CustomScroll").mCustomScrollbar("destroy");
			} else{
				$(".CustomScroll").mCustomScrollbar();
			};
		}
	}

	stickyHeader();
	stopScrollMobile();
	overflowThreeLine();
	overflowThreeLineDesc();
	js_repos_products();
	cutTotalPrice();
	js_repos_sort();
	js_repos_buying();
	js_repos_buying2();
	js_repos_autor();
	fromHeaderForMobile();
	closeFilters();
	reposeQuanity();
	initSwiperPdp();
	initSwiperCountry()
	$(window).resize(function() {
		js_repos_sort();
		stopScrollMobile();
		stickyHeader();
		fromHeaderForMobile();
		overflowThreeLine();
		overflowThreeLineDesc();
		js_repos_products();
		js_repos_buying();
		js_repos_buying2();
		js_repos_autor();
		cutTotalPrice();
		closeFilters();
		reposeQuanity();
		initSwiperPdp();
		initSwiperCountry()
	});

	$(window).scroll(function() {
		stickyHeader();
		goToTop();
	});

	var showCateg = $( ".showCategories" );

	// function addOnHover() {
	// 	showCateg.queue(function() {
	// 		showCateg.addClass("opened");
	// 		$("body").addClass("modal-open");
	// 		$("body").addClass("head_open");
	// 		showCateg.dequeue();
	// 	});
	// };
	// function removeOutHover() {
	// 	showCateg.delay(100).queue(function() {
	// 		showCateg.removeClass("opened");
	// 		$("body").removeClass("modal-open");
	// 		$("body").removeClass("head_open");
	// 		showCateg.dequeue();
	// 	});
	// };
	// if (Modernizr.touchevents) {
	
	$(document).on("click", ".goto_up", function( event ){
		$("html,body").animate({"scrollTop":0},400); 
	})

	$(document).on("click", ".bg_category", function( event ){
		$("body").removeClass("modal-open");
		$("body").removeClass("head_open");
		$(".showCategories").removeClass("opened");
  	});
  	
	$(document).on("click", ".showCategories", function( event ){
		showCateg.toggleClass("opened");
		$("body").toggleClass("modal-open");
		$("body").toggleClass("head_open");
	});
	// Show/hide My account menu



	$(document).on( "click", ".stroke_activait", function( event ){
		if($(this).hasClass("active")){
			$(this).closest(".corect_likedislike").find(".stroke_activait.active").removeClass("active");
		}else{
			$(this).closest(".corect_likedislike").find(".stroke_activait.active").removeClass("active");
			$(this).addClass("active");
		}
	});

	$(document).on( "click", "html", function( event ){
		$(".my_account").addClass("opened");
		if ($(".my_account").has(event.target).length === 0) { 
			$(".my_account").removeClass("opened");
		}
	});
	// } else {
	// 	$(document).on("click", ".hoverPreventDef", function( event ) {
	// 		event.preventDefault();
	// 	});
	// 	$( ".showCategories" ).hover(
	// 		function() { addOnHover()},
	// 		function() { removeOutHover()}
	// 	);
	// 	$( ".category_list .wrap_category_list" ).hover( 
	// 		function() { addOnHover()},
	// 		function() { removeOutHover()}
	// 	);
	// };
	// category menu
	var widthCategory = $(".wrap_category_list").width();
	setLevelAndLeft()
	function setLevelAndLeft(){ 
		$( ".sub_list" ).each(function() {
			var lavel = $( this ).parents(".sub_list").length;
			$( this ).attr("data-level", lavel +1);
			var valLavel = $( this ).data("level");
			$( this ).css({"left" : - widthCategory * valLavel - 20, "transition": "left .2s ease-out"})
		});
	};
	$(document).on("click", "[data-btn]", function( event ){
		event.preventDefault();
		var parents = $(this).parents(".custom_list").length;

		var currentList = $('[data-list=' + $(this).data('btn') + ']')
		var currentLevel = currentList.data("level");
		var posLeft = currentList.position().left;
		setLevelAndLeft();
		// positioning of the current list
		$(".onlyScroll").removeClass("onlyScroll");
		$(".fistLavel").css({"left": - currentLevel * widthCategory, "transition": "left .2s ease-out"});
		currentList.css({"left":  posLeft / widthCategory , "transition": "left .2s ease-out"}).addClass("onlyScroll");

		/// reset to the main category menu
		$(document).on("click", ".mainBack", function( event ){
			$(".onlyScroll").removeClass("onlyScroll");
			setLevelAndLeft();
			$('[data-list=' + $(this).data('btn') + ']').css({"left": widthCategory, "transition": "left .2s ease-out"});
			$(".fistLavel").css({"left": posLeft / widthCategory }).addClass("onlyScroll");
		});
	});

	$(document).on("click", ".category_list .mobileBringFriend", function( event ) {
		$(".bring_a_friend").modal("show");
		console.log("@@")
	})

// Delete an item by clicking on "X"
	$(".btnRemove").click(function(){
		var elemForRemove = $(this).closest(".canRemove")
		$(".colItems").text(+$(".colItems").text()-1)
		elemForRemove.hide(200);
		setTimeout(function() {
			elemForRemove.remove();
		}, 200)
		if ($(".colItems").text()=="0") {
			$(".allCanDeleted").addClass("hidden");
			$(".allCanWisib").removeClass("hidden");
		}
	});

	$(".deleteAllItems").click(function(){
		$(this).addClass("all_removed")
		$(".colItems").text(0)
		$(".my_watchlist  .canRemove").hide(200);
		setTimeout(function() {
			$(".my_watchlist  .canRemove").remove();
		}, 200)
		$(".allCanDeleted").addClass("hidden");
		$(".allCanWisib").removeClass("hidden");
	});

//product for counter
	// plus product
	$( ".count_box" ).each(function( event ) {
		var parentElem = $(this).closest(".count_box"); 
		var count = parentElem.find(".count").text();

		parentElem.find('.plus').click(function() {
			++count;
			parentElem.find(".count").text(count);
		})
		//  minus product
		parentElem.find('.minus').click(function() {
			if (count != 0) {
				--count;
				parentElem.find(".count").text(count);
			}
		});
	});

$(".closeCookie").click( function(event){
	$(".CookieRemove").animate({height: 0,opacity: 0}, 400);
	$(".mbCookie").removeClass("mbCookie")
	setTimeout(function() {
		$(".CookieRemove").addClass("hide");
	}, 500)
})

// For clone input on bring_friend page
	$(".cloneElem").click( function( ){
		$(".forClone").clone().removeClass("forClone").val("").insertBefore(".cloneElem");
	});
});


$(".shopDrop").click( function(event){
	event.preventDefault();
	$(this).closest(".country_list").toggleClass("open");
})

// semi_open status delete
$(".semi_open").on('click',function(){
	$(this).removeClass('semi_open').closest(".semi_open_cont").find('.semi_open_block').removeClass('semi_open_block');
})
//end semi_open status delete

//fixed indo pdp
$(window).scroll(function() {
    if (window.matchMedia('(min-width: 991px)').matches) {
        if ($(this).scrollTop() > 0) {
            $(".fixed_scrolable").addClass("fixed_info");
            if($(this).scrollTop() > +$(".sect_inform").height()+$(".sect_inform2").height()-$(".fixed_scrolable").height()){
                $(".fixed_scrolable").addClass("reposited_info").css({"top":+$(".sect_inform").height()+$(".sect_inform2").height()+30-$(".fixed_scrolable").height()})
            }else{
                $(".fixed_scrolable").removeClass("reposited_info").css({"top":"210px"});
                if (window.matchMedia('(max-height: 750px)').matches) {
                	$(".fixed_scrolable").removeClass("reposited_info").css({"top":"125px"});
                }
            }
        }else{
        	$(".fixed_info").removeClass("fixed_info");
        	$(".fixed_scrolable").removeClass("fixed_info").css({"top":"auto"});
        }
    }else{
        $(".fixed_scrolable").removeClass("fixed_info").css({"top":"auto"});
    }
});
//end fixed indo pdp

//stars clikable
$(document).on("click", ".clikableStar .star", function( event ) {
	$(".activeStar").removeClass("activeStar")
	$(this).addClass("activeStar").addClass("nextCheck");
	for (var i = 4; i >= 0; i--) {
		if ($(".nextCheck").prev().hasClass("star")) {
			var pos = $(".nextCheck").prev();
			$(".nextCheck").removeClass("nextCheck");
			pos.addClass("nextCheck");
			$(".nextCheck").addClass("activeStar")
		}
	}
	$(".nextCheck").removeClass("nextCheck");
})
//stars clikable

//filters pop
$(document).on("click", ".like_wraper", function( event ) {
	$(this).toggleClass("active");
})
//end filters pop

//like icon
$(document).on("click", ".filter_but", function( event ) {
	$(".filter_but").removeClass("active");
	$(this).toggleClass("active");

})
//end like icon

//view pop
$(document).on("click", ".bigVievAct", function( event ) {
	if (!$(this).hasClass("active")) {
		$(".view_change").removeClass("active");
		$(this).toggleClass("active");
		$(".view_changed_wraper ").addClass("bigView").removeClass("listView").removeClass("smalView");
	}
})
$(document).on("click", ".smalVievAct", function( event ) {
	if (!$(this).hasClass("active")) {
		$(".view_change").removeClass("active");
		$(this).toggleClass("active");
		$(".view_changed_wraper ").addClass("smalView").removeClass("listView").removeClass("bigView");
	}
})
$(document).on("click", ".listVievAct", function( event ) {
	if (!$(this).hasClass("active")) {
		$(".view_change").removeClass("active");
		$(this).toggleClass("active");
		$(".view_changed_wraper ").addClass("listView").removeClass("smalView").removeClass("bigView");
	}
})
//end view pop

// custom select

var selectLens = 0;

// function customFilterSelect(){
	$('.select_custom').each(function(){
		selectLens++;
	    var $this = $(this), numberOfOptions = $(this).children('option').length;
	    $this.addClass('select-hidden'); 
	    $this.wrap('<div class="select"></div>');
	    $this.after('<div class="select-styled" data-toggle="collapse" data-parent=".accordion" href="#filter'+selectLens+'"></div>');
	    var $styledSelect = $this.next('div.select-styled');
	    $styledSelect.text($this.children('option').eq(0).text());
	    var $list = $('<ul />', {
	        'class': 'select-options CustomScroll collapse',
	        'id':'filter'+selectLens
	    }).insertAfter($styledSelect);

	    for (var i = 1; i < numberOfOptions; i++) {
	        $('<li />', {
	            text: $this.children('option').eq(i).text(),
	            rel: $this.children('option').eq(i).val()
	        }).appendTo($list);
	    }
	    var $listItems = $list.children('li');
	  	// $('<span />', {'class': 'select-check'}).prependTo($listItems);
	  	$('<svg class="icon_checkbox"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-checkmark"></use></svg>').prependTo($listItems);

	    $styledSelect.click(function(e) {
	        if (window.matchMedia('(min-width: 991px)').matches) {
	       		e.stopPropagation();
		        // $('div.select-styled.activait').not(this).each(function(){
		        //     $(this).removeClass('activait').next('ul.select-options').hide();
		        // });
		        $(this).toggleClass('activait').next('ul.select-options').toggle();
		    }
	    });
    	$(".CustomScroll").mCustomScrollbar();
	    $(document).click(function() {
	    	 if (window.matchMedia('(min-width: 991px)').matches) {
		        $styledSelect.removeClass('active');
		        $(".select-styled").removeClass('activait');
		        $list.hide();
		    }
	    });
	});
// }

$(document).on("click", ".oneCheck .select-options li", function( e ) {
	e.stopPropagation();
	if ($(this).hasClass("active")){
		$(".oneCheck .select-options li").removeClass("active");
	}else{
		$(".oneCheck .select-options li").removeClass("active");
		$(this).addClass("active");	
	}
})

$(document).on("click", ".all_filters .select-options li", function( e ) {
	e.stopPropagation();
	$(this).toggleClass("active");
});


$(document).on("click", ".CLearAllFilters", function( event ) {
	$(".select-options li").removeClass("active");
})
//end custom select



// all filters open
$(document).on("click", ".ShowAllFilters", function( event ) {
	$(".all_filters").toggleClass("active")
	$(".pop_page").toggleClass("FiltestFull")
	if ($(".all_filters").hasClass("active")) {
		$(".textFilterBut").text("CLOSE ALL FILTERS")	
	}else{
		$(".textFilterBut").text("show all filters")	
	}
})
//end all filters open

// validation
$(document).on("blur", ".emailValid", function( event ) {
	if($(this).val() != '') {
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
	    if(pattern.test($(this).val())){
	    } else {
	        $(this).before("<span class='error_text'>Incorrectly entered data</span>");
	        $(this).addClass("error");
	    }
	}
})
$(document).on("blur", ".nameValid", function( event ) {
	if($(this).val() != '') {
        var pattern = /^[a-zA-Z][a-zA-Z-_\.]{1,30}$/;

	    if(pattern.test($(this).val())){
	    } else {
	    	$(this).prev().remove(".error_text")
	        $(this).before("<span class='error_text'>Incorrectly entered data</span>");
	        $(this).addClass("error");
	    }
	}
})
$(document).on("blur", ".confirmPrev", function( event ) {
	if($(this).val() != $(this).closest(".parentFoConfirm").prev(".parentFoConfirm").find(".sameData").val()){
		$(this).prev().remove(".error_text")
		if ($(this).hasClass("confirmAnoth")){
			$(this).before("<span class='error_text'>"+$(this).attr("data-conf")+"</span>");
		}else{
			$(this).before("<span class='error_text'>Must coincide</span>");
		}
        $(this).addClass("error");
	}
})
$(document).on("blur", ".validReqw", function( event ) {
   if($(this).val() == '') {
   		$(this).prev().remove(".error_text")
        $(this).addClass("error");
        $(this).before("<span class='error_text'>Should not be empty</span>");
    }
})
$(document).on("focus", ".validReqw", function( event ) {
	$(this).prev().remove(".error_text")
    $(this).removeClass("error");
    $(this).parent().find(".error_text").remove();
})
$(document).on("click", ".reqivedCheck", function( event ) {
	$(this).closest(".checkZone").find(".validReqw").each(function(){
		if ($(this).is(':invalid')) {
 			event.preventDefault();
			$(this).prev().remove(".error_text")
			$(this).addClass("error");
			$(this).before("<span class='error_text'>Incorrectly entered data</span>");
		}
		if ($(this).val()=='') {
			event.preventDefault();
			$(this).prev().remove(".error_text")
			$(this).addClass("error");
			$(this).before("<span class='error_text'>Should not be empty</span>");
		}
		if($(this).hasClass("confirmPrev")){
			if($(this).val() != $(this).closest(".parentFoConfirm").prev().find(".sameData").val()){
				event.preventDefault();
				$(this).prev().remove(".error_text")
				if ($(this).hasClass("confirmAnoth")){
					$(this).before("<span class='error_text'>"+$(this).attr("data-conf")+"</span>");
				}else{
					$(this).before("<span class='error_text'>Must coincide</span>");
				}
		        $(this).addClass("error");
			}
		}
	})
	if($(this).hasClass("modalPassChange")){
		event.preventDefault();
		if (!$(".validReqw").hasClass("error")) {
			$(".change_pass_modal").modal("show")
		}
	}
})
//end validation

// check  out bitting remember
$(document).on("click", ".billingChange", function( event ) {
	if ($(this).children("input").prop("checked")) {
		$(this).closest(".changeBilling").addClass("noRememBer");		
		var height= $("body").height(); 
		$("html,body").animate({"scrollTop":height},400); 
	}else{
		$(this).closest(".changeBilling").removeClass("noRememBer");
	}
})
// end check out bitting remember

// registration/login check_out
$(document).on("click", ".registreOnn", function( event ) {
	$(".register_login").addClass("new_acc");
})
$(document).on("click", ".loginOnn", function( event ) {
	$(".register_login").removeClass("new_acc");
})
// end registration/login check_out

$(document).on("click", ".searchOpen", function( event ) {
 $(this).closest(".search_result").toggleClass("active");
})

$(document).ready(function() {
	var title = document.title;
	var shareUrl = window.location.href;
	var description = $('meta[name="description"]').attr("content")
	$('.toReplaceLink').each(function(){
		var link = $(this).attr("href").replace("{t}",encodeURIComponent(title)).replace("{u}",encodeURI(shareUrl)).replace("{d}", encodeURIComponent(description));
		$(this).attr("href",link)
	})
})