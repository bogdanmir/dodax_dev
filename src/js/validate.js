$(function() {
	// validation if empty
		function isEmpty(selector) {
			var empty = false;
			if (selector.val() == "") {
				return empty = true;
			}
			return empty;
		};
	// END validation if empty

	// email validation
		function isEmail(selector) {
			var email = false;
			var patternEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if( !patternEmail.test($(selector).val()) ) {
				return email = true;
			}
			return email;
		};
	// END email validation

	// domen
		function isDomen(selector) {
			var domen = false;
			var patternDomen = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
			if (!patternDomen.test($(selector).val())) {
				return domen = true;
			}
			return domen;
		}
	// END domen

	// front-end validation
	var messages;
	function checkingInput (selector, type, postFunction) {
		$(".jsModalMessage .jsModalMessageText").text('');
		selector.find('.jsAlertMessage').remove();
		var countInputsRequired = selector.find('[required]').length;
		var successInputs = 0;
		messages = new Array();
		selector.find('[required]').each( function() {
			var thisName = $(this).attr('name');
			var keyName;
			for (i = 0; i < response_status.data.length; ++i) {
				for (j = 0; j < response_status.data[i].dataMessage.length; ++j) {
					if (response_status.data[i].dataMessage[j].name === thisName) {
						keyName = response_status.data[i].dataMessage[j];
					}
				}
			}
			if (isEmpty($(this)) == true) {
				messages.push({name: thisName, message: keyName.empty_form, typeMsg: 'error'});
			} else if (keyName.type == 'email' && isEmail($(this))) {
				messages.push({name: thisName, message: keyName.bademailaddress, typeMsg: 'error'});
			} else if (keyName.type == 'domen' && isDomen($(this))) {
				messages.push({name: thisName, message: keyName.bad_url, typeMsg: 'error'});
			} else {
				++successInputs;
			}

		});
		console.log(messages)
		// if (countInputsRequired === successInputs) {
		// 	if (selector.find('#g-recaptcha').length) {
		// 		var responseCaptcha = grecaptcha.getResponse();
		// 		if(responseCaptcha.length == 0) {
		// 			$('.jsErrorCaptcha').slideDown();
		// 		} else {
		// 			postFunction ();
		// 		}
		// 	} else {
		// 		postFunction ();
		// 	}
		// } else {
		// 	showMessage (selector, messages, type);
		// }
		
	}
	// front-end validation

 

	// show message (modal/collapse)
	function showMessage (selector, msgs, type) {
			
		if (type == 'modal') {
			$.each( msgs, function( i, msg ) {
				var msgError = $('.jsModalMessage .jsModalMessageText .jsAlertMessage').text();
				if (msg.typeMsg == 'error') {
					if (msg.message != msgError) {
						var text = '<h3><i class="glyphicon glyphicon-exclamation-sign"></i><span class="modal_txt jsAlertMessage">' + msg.message + '</span></h3>';
						$('.jsModalMessage .jsModalMessageText').append(text);
					}
					$('.jsModalMessage .btn').removeClass('btn-success').addClass('btn-danger');
				} else if (msg.typeMsg == 'success') {
					var text = '<h3><i class="glyphicon glyphicon-ok-sign"></i><span class="modal_txt jsAlertMessage">' + msg.message + '</span></h3>';
					$('.jsModalMessage .jsModalMessageText').append(text);
					$('.jsModalMessage .btn').removeClass('btn-danger').addClass('btn-success');
				}
			});
			return showModalError();
		} else if (type == 'collapse') {
			$.each( msgs, function( i, msg ) {
				var input = '.form-control[name="' + msg.name + '"]';
				var msgError = $(input).closest('div').find('.jsAlertMessage .alert').text();
				if (msg.message != msgError) {
					var errorBlock = '<div class="wrap_alert jsAlertMessage"><div class="alert alert-danger">' + msg.message + '</div></div>';
					selector.find(input).closest('div').append(errorBlock);
				}
				$('.jsAlertMessage').slideDown();
			});
		}
	}

	function showModalError () {
		$('.jsModalMessage').modal('show');
	}
	// END show message (modal/collapse)


	// parse response status
	var responseStatus;
	var result;
	function parseResponseStatus (responseArray) {
		responseStatus = new Array();
		$.each(responseArray, function( i, key) {
			var test = key.split(" ");
			responseStatus.push(test);
		});
		messages = new Array();
		var countResponseStatus = 0;
		var lengthResponseStatus = responseStatus.length;
		var successStatus;
		$.each(responseStatus, function( i, key) {
			if (key[0] == 'error') {
				for (i = 0; i < response_status.data.length; ++i) {
					for (j = 0; j < response_status.data[i].dataMessage.length; ++j) {
						if (response_status.data[i].dataMessage[j][key[1]]) {
							messages.push({name: response_status.data[i].dataMessage[j].name, message: response_status.data[i].dataMessage[j][key[1]], typeMsg: 'error'});
						}
					}
				}
				successStatus = false;
			} else {
				for (i = 0; i < response_status.data.length; ++i) {
					for (j = 0; j < response_status.data[i].dataMessage.length; ++j) {
						if (response_status.data[i].dataMessage[j][key[0]]) {
							messages.push({message: response_status.data[i].dataMessage[j][key[0]], typeMsg: 'success'});
						}
					}
				}
				successStatus = true;
			}
			++countResponseStatus;
		});
		result = {
			lengthResponseStatus: lengthResponseStatus,
			successStatus: successStatus,
			countResponseStatus: countResponseStatus
		}
		return result;
	}
	// END parse response status



	// submit forms
		// newsletter
		$(".loginForm").submit(function(e) {
			e.preventDefault();
			var thisForm = $(this);
			checkingInput($(".loginForm"), 'collapse', function () {
				var url = "https://" + document.domain + "/events/newsletterform.php";
				$.post(url, {
					subscriptionEmail: $("#subscriptionEmail").val(),
				}, function (response) {
					parseResponseStatus (response.status);
					if (result.countResponseStatus == result.lengthResponseStatus) {
						if (result.successStatus == false) {
							return showMessage (thisForm, messages, 'modal');
						} else {
							return showMessage (thisForm, messages, 'modal');
						}
					}
				},'json');
			});
		});
		// newsletter


	$('.modal').on('hidden.bs.modal', function (e) {
	  $('.jsModalMessageText').empty()
	})
});